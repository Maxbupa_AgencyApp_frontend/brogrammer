import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'

import {
  LandingScreen,
  WebView
} from '../screens/index'
import InitialRoute from '../screens/InitialRoute'
import Dashboard from '../screens/dashboard/Dashboard'
import CalendarsScreen from '../screens/calendar/Calendar'
import CardSlider from '../screens/slider/CardSlider'
import BottomSheet from '../screens/bottomSheet/BottomSheet'
import Descriptions from '../screens/dashboard/Descriptions'
import Chart from '../screens/charts/Chart'
import StepIndicator from '../screens/maps/StepIndicator'

const Stack = createStackNavigator()

export function MainStackNavigator(props) {
  return (
    <Stack.Navigator headerMode="none" initialRouteName={props.routeName}>
      <Stack.Screen name='LandingScreen' component={LandingScreen} />
      <Stack.Screen name='InitialRoute' component={InitialRoute} />
      <Stack.Screen name='Dashboard' component={Dashboard} />
      <Stack.Screen name='Descriptions' component={Descriptions} />
      <Stack.Screen name='Calendar' component={CalendarsScreen} />
      <Stack.Screen name='Carousel' component={CardSlider} />
      <Stack.Screen name='Chart' component={Chart} />
      <Stack.Screen name='StepIndicator' component={StepIndicator} />
      <Stack.Screen name='BottomSheet' component={BottomSheet} />
      <Stack.Screen name='WebView' component={WebView} />
    </Stack.Navigator>
  )
}