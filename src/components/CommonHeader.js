import React from 'react';
import { View, TouchableOpacity, Image, Text } from 'react-native'
import { Color, Assets } from '../../res/index';
import { useNavigation, CommonActions } from '@react-navigation/native';

export const CommonHeader = props => {

    const navigation = useNavigation();
    return (
        // <View style={[{ flexDirection: 'row', alignSelf: 'center', paddingBottom: 20, }, props.containerStyle]}>
        //     <View style={{ flex: 0.1, paddingVertical: 5, paddingRight: 5, justifyContent: 'center' }}>
        //         <TouchableOpacity
        //             onPress={props.onPress != null ? props.onPress : () => navigation.goBack()}
        //         >
        //             <Image
        //                 style={[props.leftIconStyle]}
        //                 source={props.backImage ? props.backImage : Assets.common.blackBackButton}>
        //             </Image>
        //         </TouchableOpacity>
        //     </View>
        //     <View style={{ flex: 0.8, alignItems: 'center', justifyContent: 'center', paddingTop: 5 }}>
        //         <View style={{ alignItems: 'center' }}>
        //             {props.headerTrue && <Text style={{ color: props.headerTitleColor ? props.headerTitleColor : Color.textColor.quarternary, fontSize: props.headerTitleFontsize, fontWeight: props.headerTtileFontWieght }}>{props.headerTrue}</Text>}
        //             {props.pageCount && <Text style={{ color: Color.introColor.primaryColor, fontSize: 12, fontWeight: '700' }}>{props.pageCount}</Text>}
        //         </View>
        //     </View>
        //     <View style={{ flex: 0.1 }}>
        //         {props.skip && <TouchableOpacity style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
        //             onPress={props.skiponPress}
        //         >
        //             <Text style={{ color: Color.textColor.pentaColor }}>{props.skip}</Text>
        //         </TouchableOpacity>}
        //     </View>
        // </View >

        <View style={{
            width: '100%',
            flexDirection: 'row',
            height: 50,
            alignItems: 'center',
            backgroundColor: 'white',
            shadowColor: "#000",
            shadowOffset: { width: 0, height: 4 },
            shadowOpacity: 0.23,
            shadowRadius: 2.62,
            elevation: 4
        }}>
            <TouchableOpacity onPress={props.onPress != null ? props.onPress : () => navigation.goBack()}>
                <Image style={{ marginLeft: 15, }} source={Assets.common.back}></Image>
            </TouchableOpacity>

            <View style={{
                justifyContent: 'center',
                alignItems: 'center'
            }}>
                <Text style={{ marginLeft: 30, fontSize: 24, fontWeight: 'bold', color: '#337ab7' }}>
                    {props.component_name}
                </Text>
            </View>
            <Text></Text>
        </View>
    );
}

