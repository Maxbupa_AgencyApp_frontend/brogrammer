import React from 'react'
import {
    Text,
    StyleSheet,
    Image,
    TouchableOpacity,
} from 'react-native'
import { Color } from '../../res'

export const AppButton = (
    {
        isEnabled,
        buttonStyle,
        buttonTextStyle,
        buttonText,
        isUpperCase,
        camelCaseText,
        onPress,
        showSideImage,
        imageAssets,
        imageStyle
    }) => {
    return (
        <TouchableOpacity
            style={[styles.container,
            {
                backgroundColor: isEnabled ?
                    Color.buttonColor.enableButton : Color.buttonColor.disableButton
            }, buttonStyle,]}
            activeOpacity={0.1}
            onPress={() => {
                console.log("dadahjdjh")
                return isEnabled ? onPress() : null
            }}
        >

            {showSideImage && <Image
                source={imageAssets}
                style={imageStyle} />}

            {buttonText && <Text style={[styles.textStyle, {
                fontWeight: '800',
                color: Color.secondayTextColor
            }, buttonTextStyle]}>{buttonText.toUpperCase() || 'Dummy'}</Text>}
            {isUpperCase && <Text style={[styles.textStyle, {
                fontWeight: '800',
                color: Color.secondayTextColor
            }, buttonTextStyle]}>{camelCaseText || 'Dummy'}</Text>}
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 25,
    },
    textStyle: {
        fontSize: 16
    }
});
