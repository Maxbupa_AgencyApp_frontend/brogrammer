import React, { Component } from 'react'
import { StyleSheet, View, Image, TextInput, Text, TouchableOpacity } from 'react-native'
import { TextField } from 'react-native-material-textfield'
import { Dimen } from '../../res'

export default class LiquorInputField extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isFocussed: this.props.isFocussed,
            text: (this.props.value && this.props.value.length) ? this.props.value : '',
            errorMsg: '',
            showPassword: true
        }
    }

    focus = () => this.refs.textInput.focus()

    render() {
        let isErrorOccured = (this.props.errorMessage != undefined && this.props.errorMessage.length != 0)
        const overrideLeftPadding = this.props.overrideLeftPadding || 0
        const lineWidth = (this.props.lineWidth != undefined) ? this.props.lineWidth : 0.5
        const activeLineWidth = (this.props.activeLineWidth != undefined) ? this.props.activeLineWidth : 1
        const containerBottomPadding = this.props.containerBottomPadding || 15

        const errorTextColor = (isErrorOccured == true ? (this.props.errorTextColor || 'red') : undefined)
        const borderStyle = {
            borderBottomWidth: this.state.isFocussed ? activeLineWidth : lineWidth,
            paddingBottom: containerBottomPadding,
            borderColor: errorTextColor
        }

        const errorMessageStyle = {
            marginTop: 2,
            color: errorTextColor,
            fontSize: 12,
            alignSelf: 'flex-end'
        }

        if (this.props.materialTextInput) {
            let height = this.props.height ? this.props.height : 45
            return (
                <View>
                    <View style={this.props.style}>
                        <View style={[{ flexDirection: 'row', alignItems: 'flex-end' }, borderStyle]}>
                            {this.props.leftImage && <Image source={this.props.leftImage}
                                style={{ width: 20, resizeMode: 'contain', marginLeft: 0, marginRight: 6 }} />}

                            <TextField {...this.props}
                                placeholder={undefined}
                                label={this.props.placeholder}
                                ref={'textInput'}
                                inputContainerStyle={{ paddingLeft: overrideLeftPadding, paddingRight: this.props.secureTextEntry ? 30 : 0 }}
                                labelTextStyle={{ flex: 1, paddingLeft: overrideLeftPadding, textAlign: 'justify' }}
                                titleTextStyle={{ alignSelf: 'flex-start', textAlign: 'justify' }}
                                lineWidth={0}
                                value={this.props.value}
                                onSubmitEditing={() => {
                                    if (this.props.onSubmitEditing)
                                        this.props.onSubmitEditing()
                                }}
                                secureTextEntry={this.props.secureTextEntry && this.state.showPassword}
                                activeLineWidth={0}
                                maxLength={this.props.maxLength}
                                keyboardType={this.props.keyboardType}
                                disabledLineWidth={0}
                                inputContainerPadding={4}
                                onFocus={() => this.setState({ isFocussed: true }, this.props.onFocus)}
                                onBlur={() => this.setState({ isFocussed: false }, this.props.onBlur)}
                                style={[{ fontSize: Dimen.mediumTextSize, backgroundColor: 'transparent', paddingRight: 10 }, this.props.inputStyleOverrides,
                                //  Constants.fonts.bold
                            ]}
                                containerStyle={[{ top: (this.props.topMarginOffset || -5), height, width: this.props.width || '100%', }, this.props.keepBottomMargin && { marginBottom: 15 }]}
                                onChangeText={(text) => {
                                    console.log('state text is ', text, 'sdf', this.state.text);
                                    this.setState({ text: text })

                                    if (this.props.onChangeText) this.props.onChangeText(text)
                                }}
                            />

                            <View style={{ position: 'absolute', right: 0 }}>
                                {this.props.rightImage && <Image source={this.props.rightImage}
                                    style={{ width: 20, resizeMode: 'contain', marginRight: 10, marginBottom: 20 }} />}
                            </View>
                        </View>
                        {isErrorOccured && <Text style={[errorMessageStyle, this.props.errorMessageStyle]}>{this.props.errorMessage}</Text>}

                    </View>
                    {
                        this.props.secureTextEntry &&
                        <TouchableOpacity
                            onPress={() => { this.setState({ showPassword: !this.state.showPassword }) }}
                            style={{
                                height: 100,
                                position: 'absolute', right: 40, justifyContent: 'center', alignItems: 'center'
                            }} >
                            <Image style={{
                                width: 20,
                                height: 13,
                                marginLeft: 10,
                                // tintColor: (this.state.showPassword ? colors.primaryColor : colors.grey)
                            }}
                                // source={this.state.showPassword ? image.login.showPassword : image.login.dontShowPassword}
                            />
                        </TouchableOpacity>
                    }
                </View>
            )
        } else {
            let height = 30
            let width = this.props.width || '100%'
            let defaultContainerStyle = {
                width,
                height,
                borderBottomWidth: (this.state.isFocussed ? this.props.activeLineWidth : this.props.lineWidth) || 0.2,
                borderColor: 'lightgrey',
            }
            return (
                <View style={[defaultContainerStyle, this.props.containerStyle]}>
                    <TextInput {...this.props} width={undefined} ref={'textInput'}
                        onFocus={() => this.setState({ isFocussed: true })} onBlur={() => this.setState({ isFocussed: false })} />
                </View>
            )
        }
    }

    componentDidMount() {
        if (this.props.isFocussed) this.focus()
    }

}
