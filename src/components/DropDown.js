

import React, { Component } from 'react'
import { StyleSheet, View, Image, TextInput, Text, TouchableOpacity } from 'react-native'
import { TextField } from 'react-native-material-textfield'
import { Dimen } from '../../res'
import { LiquorInputField } from './index'
import { Color, Assets, Strings } from '../../res/index';

export default class DropDown extends Component {
    constructor(props) {
        super(props)
        this.state = {
            educationSelected: 'Hello'
        }
    }
    render() {
        return (<TouchableOpacity
            style={[{ flex: .5, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', borderBottomWidth: 1, borderBottomColor: Color.borderColor.primaryColor }, this.props.style]}
            onPress={() => { this.props.onPress() }}>
            <LiquorInputField
                value={this.props.values}
                placeholder={this.props.placeHolder}
                fontSize={16}
                editable={false}
                autoCapitalize='none'
                style={{ paddingTop: 10 }}
                labelEnabled={true}
                tintColor={Color.borderColor.secondaryColor}
                lineWidth={0}
                activeLineWidth={0}
                width='90%'
                // keyboardType='default'
                materialTextInput
            />
            <Image style={{ marginLeft: 5, marginTop: 20, marginRight: 10 }} source={Assets.common.dropDown} />
        </TouchableOpacity>
        )
    }
}
