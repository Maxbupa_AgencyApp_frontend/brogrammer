import Toast from 'react-native-root-toast';
import { ToastAndroid, Platform } from 'react-native'
import AsyncStorage from "@react-native-community/async-storage";
import Constants from '../../res/Constants';
import String from '../../res/String';

export default class Session {
    static sharedInstance = Session.sharedInstance == null ? new Session() : Session.sharedInstance;
    id = undefined
    token = undefined
    name = undefined
    email = undefined
    gender = undefined
    profilePic = undefined
    kitchenPic = undefined
    kithenName = undefined
    kitchenContactNo = undefined
    kitchenEmailAddress = undefined
    kitchenLocation = undefined
    City = undefined
    Pincode = undefined
    fssai_doc = undefined
    openingTime = undefined
    closingTime = undefined
    minDeliveryTime = undefined
    deliveryRange = undefined
    deliverType = undefined
    kitchenImages = undefined
}