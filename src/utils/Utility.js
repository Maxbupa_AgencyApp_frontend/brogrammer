import Toast from 'react-native-root-toast';
import { ToastAndroid, Platform } from 'react-native'
import AsyncStorage from "@react-native-community/async-storage";
import String from '../../res/String';
import Constants from '../../res/Constants';

export default class Utility {
    static sharedInstance = Utility.sharedInstance == null ? new Utility() : Utility.sharedInstance;
    HOC = undefined
    navigation = undefined

    validateEmptyField = (fieldValue, errorText) => {
        if (fieldValue.trim() == '') {
            showToast(errorText)
            return false
        }
        return true
    }

    validateEmptyField(fieldValue, errorText) {
        console.log(fieldValue, errorText);
        if (fieldValue.trim() == '') {
            showToast(errorText)
            return false
        }
        return true
    }


    validateMobileNumber(fieldValue, regExr, errmsg) {
        // const regEx = regExr
        if (this.validateRegex(regExr, fieldValue)) {
            return true
        } else {
            showToast(errmsg)
            return false
        }
    }

    validateName(name) {
        if (!this.validateRegex(Constants.regex.alphabet, name)) {
            showToast(String.toastMsgs.studentReg.validName)
            return false
        }
        return true
    }


    validateRegex = (regex, text) => {
        let regx = new RegExp(regex)
        console.log("regx" + regx)
        console.log("text" + text)
        console.log("regx.test(text)" + regx.test(text))
        return regx.test(text)
    }

    validateEmailAddress(email) {
        if (!this.validateRegex(Constants.regex.email, email)) {
            showToast(String.toastMsgs.login.validEmail)
            return false
        }
        return true
    }

    validatePincode(pincode) {
        if (pincode.length < 6) {
            showToast(String.toastMsgs.login.valid_pincode)
            return false
        }
        return true
    }

    static log(msg, ...options) {
        if (__DEV__) {
            console.log(msg, options);
        }
    }

    isApplogicLogin = () => {
        ApplozicChat.isUserLogIn((response) => {
            if (response == "true") {
                console.log("CHCEK LOGGED IN")
                /// User is logged in already
            } else {
                console.log("CHCEK LOGGED OUT")
                /// User is not logged in yet.
            }
        })
    }

    loginApplozic = (userData, name) => {
        console.log("userData" + JSON.stringify(userData))
        ApplozicChat.login(userData, (error, response) => {
            if (__DEV__) console.log('login_applozic_error', error)
            if (__DEV__ && error != null) showToast('Unable to login in Applozic  ' + error)
            if (__DEV__) console.log('login_applozic_response', response)
        })
    }

    //  Applozic Login Function
    //  loginApplozic = (emailId, name) => {
    //     ApplozicChat.login({
    //         'userId': emailId,
    //         'displayName': name
    //     }, (error, response) => {
    //         if (__DEV__) console.log('login_applozic_error', error)
    //         if (__DEV__ && error != null) this.showToast('Unable to login in Applozic')
    //         if (__DEV__) console.log('login_applozic_response', response)
    //     })
    // }

    logoutApplozic = () => {
        ApplozicChat.logoutUser((error, response) => {
            if (__DEV__) console.log('logout_applozic_error' + error);
            if (__DEV__) console.log('logout_applozic_response' + response);
        });
    }


    appLogoutHandler = async () => {
        Session.sharedInstance.userDetails = ""
        Session.sharedInstance.id = undefined
        Session.sharedInstance.token = undefined
        Session.sharedInstance.name = undefined
        Session.sharedInstance.email = undefined
        Session.sharedInstance.imageUrl = undefined
        Session.sharedInstance.isTutor = undefined
        Session.sharedInstance.countryCode = undefined
        Session.sharedInstance.userDetails = undefined
        Session.sharedInstance.countryName = undefined
        Session.sharedInstance.countryCodeObj = undefined

        await AsyncStorage.setItem(AsyncStorageValues.token, "")
        await AsyncStorage.setItem(AsyncStorageValues.userData, "")
        await AsyncStorage.setItem(AsyncStorageValues.isTutor, "")
        await AsyncStorage.setItem(AsyncStorageValues.userDetails, "")
        Utility.sharedInstance.navigation.navigate(Constants.routeName.login)
    }


    facebookLogin = async () => {
        let data
        data = await LoginManager.logInWithPermissions(["public_profile", "email"])
            .then(async (result) => {
                if (result.isCancelled) {
                    showToast("Login was cancelled");
                    return null
                }
                return await AccessToken.getCurrentAccessToken();
            })
            .then(async (data) => {
                NetworkManager.networkManagerInstance.progressBarRequest(false)

                return await fetch('https://graph.facebook.com/v2.5/me?fields=name,email,picture.width(200),friends&access_token=' + data.accessToken.toString())
                    .then((response) => response.json())
                    .then((json) => {
                        return json
                    })
                    .catch(() => {
                        if (__DEV__) console.log('ERROR GETTING DATA FROM FACEBOOK')
                    })
            })
            .catch(err => {
                NetworkManager.networkManagerInstance.progressBarRequest(false)
                if (__DEV__) console.log("fail", err);
            });

        return data
    }


    async _configureGoogleSignIn() {
        await GoogleSignin.hasPlayServices();
        GoogleSignin.configure({
            webClientId: '464550427948-m6ton896thoeubgmpeigmfaeudgqrqbc.apps.googleusercontent.com',
            offlineAccess: false
        })
    }



    googleSignin = async () => {
        try {
            const userInfo = await GoogleSignin.signIn()
            GoogleSignin.revokeAccess()
            return userInfo

        } catch (error) {
            if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                if (__DEV__) console.log('this is cancel progressed')
            } else if (error.code === statusCodes.IN_PROGRESS) {
                if (__DEV__) console.log('this is ')
            } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                if (__DEV__) console.log('playServices is not availabel')
            } else {
                if (__DEV__) console.log('this error', error.toString())
            }

        }
    }



    _storeData = async (key, data, msg) => {

        if (__DEV__) console.log("_storeData_storeData_storeData" + msg)

        try {
            await AsyncStorage.setItem(
                key,
                JSON.stringify(data)
            );
        } catch (error) {
            if (__DEV__) console.log(error)
        }
    };

    async _retrieveData(key) {
        try {
            const value = await AsyncStorage.getItem(key);
            if (value !== null) {
                // We have data!!
                console.log("Value " + value);
                return value;
            }
        } catch (error) {
            // Error retrieving data
        }
    };


    formatDate(date, format) {
        return moment(date).format(format == undefined ? 'DD MMMM, YYYY' : format)
    }

    async backPressAppExit() {
        if (Platform.OS == "ios") {
            RNExitApp.exitApp()
        } else {

            BackHandler.exitApp()
        }

    }



    formatTimeinHour(dateAndTime, format) {

        let startTime = moment(new Date(dateAndTime).getTime()).format('h:mm A').replace('PM', '')
        let endTime = moment(new Date(dateAndTime).getTime()).add(1, 'hour').format('h:mm A')
        let time = startTime + '- ' + endTime

        return time
    }


    getStatus(status) {
        switch (status) {
            case 'Active':
                return 'Active'
            case 'Upcoming':
                return 'Applied'

            default:
                break;
        }
    }




}




export async function showToast(message) {
    if (Platform.OS === 'ios') {
        let toast = Toast.show(message, {
            duration: Toast.durations.LONG,
            position: -50,
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0,
            onShow: () => {

            },
            onShown: () => {

            },
            onHide: () => {

            },
            onHidden: () => {

            },
        });
        setTimeout(function () {
            Toast.hide(toast);
        }, 5000);
    } else {
        ToastAndroid.show(message, ToastAndroid.SHORT);
    }
}

export async function _storeData(key, data, msg) {

    if (__DEV__) console.log(msg)

    try {
        await AsyncStorage.setItem(
            key,
            JSON.stringify(data)
        );
    } catch (error) {
        if (__DEV__) console.log(error)
    }
};

export async function _retrieveData(key) {
    try {
        const value = await AsyncStorage.getItem(key);
        if (value !== null) {
            // We have data!!
            console.log("Value " + value);
            return value;
        }
    } catch (error) {
        // Error retrieving data
    }
};
