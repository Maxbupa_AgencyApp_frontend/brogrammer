import React from 'react'
import { SafeAreaView, View, Text, ScrollView } from 'react-native'
import { AreaChart, Grid, StackedAreaChart, BarChart, StackedBarChart, LineChart, PieChart } from 'react-native-svg-charts'
import * as shape from 'd3-shape'
import { HeaderBackButton } from '@react-navigation/stack';
import { CommonHeader } from '../../components/CommonHeader';

const data = [50, 10, 40, 95, -4, -24, 85, 91, 35, 53, -53, 24, 50, -20, -80]

const data1 = [
    {
        month: new Date(2015, 0, 1),
        apples: 3840,
        bananas: 1920,
        cherries: 960,
        dates: 400,
    },
    {
        month: new Date(2015, 1, 1),
        apples: 1600,
        bananas: 1440,
        cherries: 960,
        dates: 400,
    },
    {
        month: new Date(2015, 2, 1),
        apples: 640,
        bananas: 960,
        cherries: 3640,
        dates: 400,
    },
    {
        month: new Date(2015, 3, 1),
        apples: 3320,
        bananas: 480,
        cherries: 640,
        dates: 400,
    },
]

const colors = ['#8800cc', '#aa00ff', '#cc66ff', '#eeccff']
const keys = ['apples', 'bananas', 'cherries', 'dates']
const svgs = [
    { onPress: () => console.log('apples') },
    { onPress: () => console.log('bananas') },
    { onPress: () => console.log('cherries') },
    { onPress: () => console.log('dates') },
]

const fill = 'rgb(134, 65, 244)'
const data2 = [50, 10, 40, 95, -4, -24, null, 85, undefined, 0, 35, 53, -53, 24, 50, -20, -80]

const data3 = [
    {
        month: new Date(2015, 0, 1),
        apples: 3840,
        bananas: 1920,
        cherries: 960,
        dates: 400,
        oranges: 400,
    },
    {
        month: new Date(2015, 1, 1),
        apples: 1600,
        bananas: 1440,
        cherries: 960,
        dates: 400,
    },
    {
        month: new Date(2015, 2, 1),
        apples: 640,
        bananas: 960,
        cherries: 3640,
        dates: 400,
    },
    {
        month: new Date(2015, 3, 1),
        apples: 3320,
        bananas: 480,
        cherries: 640,
        dates: 400,
    },
]

const colors1 = ['#7b4173', '#a55194', '#ce6dbd', '#de9ed6']
const keys2 = ['apples', 'bananas', 'cherries', 'dates']

const data4 = [50, 10, 40, 95, -4, -24, 85, 91, 35, 53, -53, 24, 50, -20, -80]

const data5 = [50, 10, 40, 95, -4, -24, 85, 91, 35, 53, -53, 24, 50, -20, -80]

const randomColor = () => ('#' + ((Math.random() * 0xffffff) << 0).toString(16) + '000000').slice(0, 7)

const pieData = data5
    .filter((value) => value > 0)
    .map((value, index) => ({
        value,
        svg: {
            fill: randomColor(),
            onPress: () => console.log('press', index),
        },
        key: `pie-${index}`,
    }))

export default class AreaChartExample extends React.PureComponent {
    render() {

        return (

            <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>

                <CommonHeader />

                <ScrollView style={{ flex: 1, paddingHorizontal: 20 }} >
                    <View style={{ alignItems: 'center', paddingVertical: 15 }}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#4A4A4A' }}>
                            {'AreaChart'}
                        </Text>
                    </View>

                    <AreaChart
                        style={{ height: 200 }}
                        data={data}
                        contentInset={{ top: 30, bottom: 30 }}
                        curve={shape.curveNatural}
                        svg={{ fill: 'rgba(134, 65, 244, 0.8)' }}
                    >
                        <Grid />
                    </AreaChart>

                    <View style={{ alignItems: 'center', paddingVertical: 15 }}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#4A4A4A' }}>
                            {'StackedAreaChart'}
                        </Text>
                    </View>

                    <StackedAreaChart
                        style={{ height: 200, paddingVertical: 16 }}
                        data={data1}
                        keys={keys}
                        colors={colors}
                        curve={shape.curveNatural}
                        showGrid={false}
                        svgs={svgs}
                    />

                    <View style={{ alignItems: 'center', paddingVertical: 15 }}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#4A4A4A' }}>
                            {'BarChart'}
                        </Text>
                    </View>

                    <BarChart style={{ height: 200 }} data={data2} svg={{ fill }} contentInset={{ top: 30, bottom: 30 }}>
                        <Grid />
                    </BarChart>

                    <View style={{ alignItems: 'center', paddingVertical: 15 }}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#4A4A4A' }}>
                            {'LineChart'}
                        </Text>
                    </View>

                    <LineChart
                        style={{ height: 200 }}
                        data={data4}
                        svg={{ stroke: 'rgb(134, 65, 244)' }}
                        contentInset={{ top: 20, bottom: 20 }}
                    >
                        <Grid />
                    </LineChart>

                    <View style={{ alignItems: 'center', paddingVertical: 15 }}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#4A4A4A' }}>
                            {'PieChart'}
                        </Text>
                    </View>

                    <PieChart style={{ height: 200 }} data={pieData} />

                </ScrollView>
            </SafeAreaView>

        )
    }
}