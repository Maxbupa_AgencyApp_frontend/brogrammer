import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    StyleSheet,
    Image,
    Dimensions,
    SafeAreaView,
    ScrollView,
    Linking
} from 'react-native';
import { connect } from "react-redux";
import { HeaderBackButton } from '@react-navigation/stack';
import { TextField } from 'react-native-material-textfield';

import { Assets } from '../../../res/index';
import { AppButton } from '../../components/AppButton';
import { API_CALL } from "../../redux/Actions";
import { CommonHeader } from '../../components/CommonHeader';

const { width } = Dimensions.get('window');

class Descriptions extends Component {
    constructor(props) {
        super(props)
        this.state = {
            item: this.props.route.params.item,
        }
    }

    render() {
        let { item } = this.state;
        console.log('this.props.route.params.item', this.props.route.params.item)
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }} >
                <View style={styles.container}>
                    <View style={{ flex: 1 }}>

                        <CommonHeader
                            component_name={item.component_name}
                        />

                        {this.renderViewTickets()}
                    </View>
                </View>
            </SafeAreaView>
        )
    }

    renderViewTickets = () => {
        let { item } = this.state;
        return (
            <View style={styles.container} >

                <ScrollView style={styles.container}>
                    <View style={{ paddingHorizontal: 20 }} >

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, alignItems: 'center' }}>

                            <View style={{}}>
                                <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#4A4A4A' }}>
                                    {'Saved Efforts'}
                                </Text>
                            </View>

                            <View style={{ alignSelf: 'center' }}>
                                <Text style={{ marginTop: 5, fontSize: 28, fontWeight: '600', color: '#0C8188' }}>
                                    {((item.actual_build_time - item.integration_time) * 100 / item.actual_build_time).toFixed(0)}{'%'}
                                </Text>
                            </View>
                        </View>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <View style={{ width: '45%' }}>
                                <TouchableOpacity
                                    disabled
                                    onPress={() => { this.showDatePicker() }}
                                    style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} >
                                    <View style={{ flex: 1 }}>
                                        <TextField
                                            editable={false}
                                            label="Integration time"
                                            value={item.integration_time}
                                            autoCapitalize='none'
                                            onChangeText={(date) => this.setState({ created_at: date })}
                                        />
                                    </View>
                                    {/* <Image style={{ marginTop: 20, width: 30, height: 30 }} source={Assets.common.date} /> */}
                                </TouchableOpacity>
                            </View>
                            <View style={{ width: '45%' }}>
                                <TouchableOpacity
                                    disabled
                                    onPress={() => { this.showDatePicker() }}
                                    style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }} >
                                    <View style={{ flex: 1 }}>
                                        <TextField
                                            editable={false}
                                            label="Actual build time"
                                            value={item.actual_build_time}
                                            autoCapitalize='none'
                                            onChangeText={(date) => this.setState({ modified_at: date })}
                                        />
                                    </View>
                                    {/* <Image style={{ marginTop: 20, width: 30, height: 30 }} source={Assets.common.date} /> */}
                                </TouchableOpacity>
                            </View>

                        </View>

                        <View style={{ marginTop: 20 }}>
                            <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#4A4A4A' }}>
                                {'Description'}
                            </Text>
                        </View>
                        <View style={{ marginTop: 10 }}>
                            <Text style={{ textAlign: 'justify', fontSize: 14, fontWeight: '600', color: '#818181' }}>
                                {item.long_description}
                            </Text>
                        </View>

                        <View style={{ marginTop: 20 }}>
                            <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#4A4A4A' }}>
                                {'Integration guide'}
                            </Text>
                            <View style={{ marginTop: 10 }}>
                                <Text style={{ color: '#337ab7' }}
                                    onPress={() => Linking.openURL(item.url)}>
                                    {item.url}
                                </Text>
                            </View>
                        </View>

                        {item.image ? <View style={{ marginTop: 20 }}>
                            <View>
                                <Text style={{ fontSize: 18, fontWeight: 'bold', color: '#4A4A4A' }}>
                                    {'Showcase'}
                                </Text>
                            </View>
                            <View>
                                <Image
                                    style={{ width: '100%' }}
                                    resizeMode='contain'
                                    source={item.image ? item.image : null}
                                />
                            </View>
                        </View> : null}

                        <View style={{ width: '100%', marginBottom: 20 }} >
                            <AppButton
                                buttonTextStyle={{ color: 'white' }}
                                buttonStyle={styles.mainButton}
                                onPress={() => { this.props.navigation.navigate(item.component_name) }}
                                isEnabled={true}
                                buttonText={'Demo'}
                            />
                        </View>

                    </View>
                </ScrollView >

            </View >
        );
    }

};

const mapStateToProps = () => {
    return {
        // 
    };
};
const mapDispatchToProps = dispatch => {
    return {
        apiDispatcher: (data) => dispatch({ type: API_CALL, data }),
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Descriptions);

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    header: {
        height: 60,
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 4 },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4
    },
    itemContainer: {
        flex: 1,
        marginHorizontal: 22,
        paddingBottom: 15,
        width: width - 46,
        backgroundColor: 'white',
        borderRadius: 10,
        marginTop: 20
    },
    mainButton: {
        width: '80%',
        flexDirection: 'row',
        alignSelf: 'center',
        marginTop: 15,
        borderWidth: 1,
        borderColor: 'gray',
        backgroundColor: '#337ab7',
    },
});