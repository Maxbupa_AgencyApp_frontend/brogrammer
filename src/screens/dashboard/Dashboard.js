import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
    StyleSheet,
    Image,
    Dimensions,
    TouchableWithoutFeedback,
    SafeAreaView,
    Keyboard,
    FlatList
} from 'react-native';
import { connect } from "react-redux";

import { Assets } from '../../../res/index';
import { API_CALL } from "../../redux/Actions";

const { width } = Dimensions.get('window');

data = [
    {
        "component_name": "Calendar",
        "time_save": "20",
        "integration_time": 1,
        "actual_build_time": 4,
        "short_description": "This module includes various customizable calendar components.",
        "long_description": "These components provide the various calendar format and highly customizable components like Date marking, Multi-Dot marking, Multi-period marking, Customizing look & feel, Customize days titles with disabled styling, Overriding day component, Horizontal CalendarList",
        "url": "https://github.com/wix/react-native-calendars",
        "image": Assets.common.calendar
    },
    {
        "component_name": "Carousel",
        "time_save": "20",
        "integration_time": 1,
        "actual_build_time": 3,
        "short_description": "This module includes various customizable carousel components.",
        "long_description": "These components provide the various calendar format and highly customizable components like ParallaxImage component, Pagination component, Custom interpolations etc",
        "url": "https://github.com/archriss/react-native-snap-carousel",
        "image": Assets.common.carousal
    },
    {
        "component_name": "BottomSheet",
        "time_save": "20",
        "integration_time": 0.5,
        "actual_build_time": 3,
        "short_description": "This module includes various customizable bottom-sheet components.",
        "long_description": "These components provide the various calendar format and highly customizable components  and has lot of features as Super Lightweight Component, Customize Whatever You Like, Support Drag Down Gesture, Support All Orientations, Smooth Animation and Zero Configuration",
        "url": "https://github.com/nysamnang/react-native-raw-bottom-sheet",
        "image": Assets.common.bottom_sheet
    },
    {
        "component_name": "Chart",
        "time_save": "20",
        "integration_time": 1,
        "actual_build_time": 4,
        "short_description": "This module includes various customizable chart-sheet components.",
        "long_description": "These components provide the various chart format and highly customizable components  and has lot of features as per chart Component, Customize Whatever You Like, Support Drag Down Chart Type, Support All Orientations, Smooth Animation and Zero Configuration",
        "url": "https://github.com/JesperLekland/react-native-svg-charts",
        "image": ''
    },
    {
        "component_name": "StepIndicator",
        "time_save": "20",
        "integration_time": 0.5,
        "actual_build_time": 3,
        "short_description": "This module includes various customizable StepIndicator components.",
        "long_description": "These components provide the various StepIndicator format and highly customizable components  and has lot of features as per tracking  any flow, Customize Whatever You Like, Support Drag Down StepIndicator Type, Support All Orientations, Smooth Animation and Zero Configuration",
        "url": "https://github.com/24ark/react-native-step-indicator#readme",
        "image": Assets.common.HorizontalStepIndicator
    }
]

class Dashboard extends Component {
    constructor(props) {
        super(props)
        this.state = {
            mobileNo: '',
            mobileNoLength: 10,
            data: [1, 2, 3, 4],
            selectedRegex: "[0-9]{10}",
            countryCodeSelected: '+91',
            countryCodeDropownData: [],
            countryCodeObj: {},
            dropdown: false,
            flag: 'IN',
            selectedCountry: 'India',
        }
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }} >
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <View style={{ flex: 1 }}>
                        <View style={styles.header}>
                            <Image
                                resizeMode='contain'
                                style={{ height: 60, width: '70%' }}
                                source={Assets.common.logo} />
                        </View>
                        {this.renderCardLists()}
                    </View>
                </TouchableWithoutFeedback>
            </SafeAreaView>
        )
    }

    renderCardLists = () => {
        return (
            <View style={{ flex: 1, paddingBottom: 20 }}>
                <FlatList
                    style={{ height: '110%', paddingBottom: 20, marginBottom: 20 }}
                    data={data}
                    ListEmptyComponent={this.ListEmptyTicketComponent}
                    renderItem={({ item, index }) => this.renderViewTickets(item, index)}
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
        )
    }

    renderViewTickets = (item, index) => {
        return (
            <View style={[styles.itemContainer, { marginBottom: (index == 4) ? 20 : 0 }]}>

                <TouchableOpacity
                    onPress={() => {
                        this.props.navigation.navigate('Descriptions', { item } = { item }, index = { index })
                    }}>

                    <View style={{ paddingHorizontal: 20 }}>

                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                            <View style={{ flex: 3 }} >
                                <Text numberOfLines={2} style={{ fontSize: 18, fontWeight: 'bold', color: '#4A4A4A' }}>
                                    {item.component_name}
                                </Text>
                            </View>
                            <View style={{ flex: 1, alignSelf: 'center' }}>
                                <Text style={{ marginTop: 5, fontSize: 28, fontWeight: '600', color: '#0C8188' }}>
                                    {/* {'20%'} */}
                                    {((item.actual_build_time - item.integration_time) * 100 / item.actual_build_time).toFixed(0)}{'%'}
                                </Text>
                            </View>
                        </View>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 10 }}>

                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={Assets.common.time} />
                                <View style={{ paddingLeft: 10 }}>
                                    <Text style={{ fontSize: 14, fontWeight: '600', color: '#4A4A4A' }}>
                                        {'Integration time'}
                                    </Text>
                                    <Text style={{ marginTop: 5, fontSize: 12, fontWeight: '600', color: '#AAAAAA' }}>
                                        {item.integration_time}{' MDs'}
                                    </Text>
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                                <Image source={Assets.common.time} />
                                <View style={{ paddingLeft: 10 }}>
                                    <Text style={{ fontSize: 14, fontWeight: '600', color: '#4A4A4A' }}>
                                        {'Actual build time'}
                                    </Text>
                                    <Text style={{ marginTop: 5, fontSize: 12, fontWeight: '600', color: '#AAAAAA' }}>
                                        {item.actual_build_time}{' MDs'}
                                    </Text>
                                </View>
                            </View>
                        </View>

                        <View style={{ marginTop: 10 }} >
                            <Text style={{ fontSize: 16, fontWeight: 'normal', color: '#4A4A4A' }}>{item.short_description}</Text>
                        </View>

                    </View>
                </TouchableOpacity>
            </View >
        );
    }

};


const mapStateToProps = () => {
    return {
        // 
    };
};
const mapDispatchToProps = dispatch => {
    return {
        apiDispatcher: (data) => dispatch({ type: API_CALL, data }),
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    header: {
        height: 80,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 4 },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4
    },
    itemContainer: {
        flex: 1,
        marginHorizontal: 22,
        paddingBottom: 15,
        width: width - 46,
        backgroundColor: 'white',
        borderRadius: 10,
        marginTop: 20,
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.23,
        shadowRadius: 2.62,
        elevation: 4,
    }
});