import React, { Component } from 'react';
import { View, StyleSheet, ImageBackground, Image, Text } from 'react-native';
import { _storeData, _retrieveData } from '../utils/Utility';
import { Assets } from '../../res/index';

export default class InitialRoute extends Component {

    async componentDidMount() {
        setTimeout(async () => {
            this.props.navigation.replace('Dashboard')
            return
        }, 3000)
        return
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <View style={styles.container}>
                    <Image
                        style={styles.imageSubContainer}
                        source={Assets.common.logo}
                        resizeMode={"contain"}
                    />
                    <Text style={{ fontSize: 20, paddingTop: 10, color: '#363636', fontWeight: '500' }}>Affle Reusable Components</Text>
                </View>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        height: '100%',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center'
    },
    imageSubContainer: {
        width: 185,
        height: 127
    }
});