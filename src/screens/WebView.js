import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { WebView } from 'react-native-webview';

export default class MyWebComponent extends Component {
    render() {
        return <WebView source={{ uri: 'http://34.86.252.172:3000/public/hubspot.html' }}
            ref={ref => { this.webView = ref; }}
            onError={() => { this.webView.reload(); }} />
    }
}