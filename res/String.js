
/**
 * 
 * Strings which will be used in the UI screens should be declared in this file.
 */
export default {
    login: {
        buttonText: 'Button Text',
    },
    verifyOtp: {
        verifyOtp: 'Verify OTP',
        wrongNumber: 'Wrong Number?',
        verify: 'Click To Verify',
        resendOtp: "RESEND OTP",
        weHave: 'We have sent you a text, with a code, to',
        pleaseEnter: 'please enter the 6-digt code.',
    },
    app: {
        apiButtonText: 'Test API via Redux',
        apiResponseText: 'API Response received',
    },
    select_lang: {
        plz_select_lang: 'Please select your language',
        plz_select_lang_hindi: 'कृपया भाषा चयन करें',
    },
    userType: {
        what: 'I am here to',
        buyer: 'Buy Food',
        seller: 'Sell Food',
        buyserStr: 'buyer',
        sellerStr: 'seller'
    },
    noInternet: {
        btnText: "Retry",
        subHeader: "Uh oh!",
        msg1: "No internet connection found.",
        msg2: " Check your connection or try again later."

    },
    studentSignUp: {
        enter_mobile_number: 'Enter Mobile Number',
        term_cond: 'By continuing, you are agree to our',
        term_cond1: 'Terms and Conditions ',
        and: 'and',
        privacy_policy: 'Privacy Policy',
        location_perm: 'Location Permission',
        fssai_documents: 'FSSAI Documents',
        fssai_document_contact: 'Contact us if you don\'t have FSSAI\nDocument. We will support you with\nyour application.',
        enable_location: 'Please enable your location to get better\nsearch results in your area.',
        skip_for_now: 'Skip for now',
        allow_location: 'Allow Location',
        contact_us: 'Contact Us',
        waiting_for_approval: 'Waiting for approval',
        waiting_for_approval_desc: 'Your profile account is waiting for our\nadministrator approval. As soon as you’re\napproved, we will notify you.',
        okay: 'Okay',
        signup_to_goodfood: 'Sign up to Good Food',
        uploadPicture: 'Upload a Picture',
        takePicture: 'Take a Picture',
        cancel: 'Cancel',
        gender: 'Gender',
        deliveryType: 'DeliveryType',
        fullName: 'Seller Name',
        emailAddress: 'Email Address',
        continue: 'Continue',
        registerWithFacebook: 'Facebook',
        registerWithGoogle: 'Google',
        kitchen_name: 'Kitchen Name',
        kitchen_conatctno: 'Kitchen Contact No.',
        kitchen_email_address: 'Kitchen Email Address',
        kitchen_location: 'Kitchen Location',
        kitchen_city: 'City',
        kitchen_pincode: 'Pincode',
        opening_time: 'Opening Time',
        closing_time: 'Closing Time',
        min_del_time: 'Minimum Delivery Time',
        delivert_area: 'Delivery Area',
        upload_fssai_doc: 'Upload FSSAI Document',
        upload_image: 'Upload kitchen Images',
        dont_have_fssai: 'Don\'t have FSSAI Document?',
        continue2: 'Continue 2/3',
        continue3: 'Continue 3/3'
    },
    Intro: {
        buttonText: 'Verify Mobile',
        discoverTeacherHeader: 'Search for the favourite\nKitchen near you',
        discoverDesc: 'Lorem Ipsum simply dummy text of the printing and is typesetting industry. Lorem Ipsum since the 1500s',
        bookSessionHeader: 'Browse the menu and\ndirectly order from the app',
        bookSessionDesc: 'Lorem Ipsum simply dummy text of the printing and is typesetting industry. Lorem Ipsum since the 1500s',
        enjoySessionHeader: 'Fast delivery to your\ndesired place',
        enjoySessionDesc: 'Lorem Ipsum simply dummy text of the printing and is typesetting industry. Lorem Ipsum since the 1500s',

        seller_header1: 'Create your kitchen and start receiving orders',
        seller_header2: 'Manage your orders as per your need',
        seller_header3: 'View your daily, weekly and monthly business',

        skip: 'SKIP'
    },
    popUpMessage: {
        exitApp: {
            header: "Confirmation!",
            desc: 'Would you like to exit the application?',
        },
        logout: {
            header: 'Logout',
            desc: 'Would you like to logout from application',
        },
        button: {
            yes: 'YES',
            no: 'NO',
        },
        bookSession: {
            desc: 'Are you sure you want to request this session?'
        }
    },
    toastMsgs: {
        login: {
            validMobileNo: 'Please enter valid mobile number.',
            uploadImage: 'Please choose image first.',
            enterName: 'PLease enter your name.',
            enterEmail: 'Please enter your Email-Id',
            validEmail: 'Please enter valid Emaild-Id',
            enterKitchenName: 'PLease enter your kitchen name.',
            enterPhone: 'Please enter phone number',
            validPhone: 'Please enter valid Phone',
            enterKitchenPhone: 'Please enter kitchen phone number',
            enter_location: 'Please select your kitchen location',
            enter_city: 'Please select your kitchen city',
            enter_pincode: 'Please enter your pincode',
            valid_pincode: "Please enter valid pincode",
            upload_fssai_doc: "Please Upload FSSAI Doc"
        }
    }
}