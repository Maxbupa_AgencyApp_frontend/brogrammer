
/**
 * 
 * Colors which will be used in the complete application should be declared in this file.
 */
export default {
    white: '#ffffff',
    black: '#000000',
    primaryColor: '#F37021',
    secondaryColor: '#E60045',
    tertiaryColor: '#5065B1',
    appBackgroundColor: '#ffffff',
    primaryTextColor: '#363636',
    secondayTextColor: '#ffffff',
    buttonColor: {
        borderColor: '#E5E5E5',
        borderColorTransparent: '#E5E5E5ff',
        disableButton: '#E2E2E2',
        enableButton: '#F37021',
        fssai_btn:'#FFFBEE'
    },
    borderColor: {
        primaryColor: '#AAAAAA',
        secondaryColor: '#0C61AF',
        tertiaryColor: '#E5E5E5'

    },
    introColor: {
        primaryColor: '#FFCB27',
        activeBackground: '#FCF5E9',
        appliedBackgroundColor: '#F2EDFF',
        secondaryColor: '#95D1A1',
        tertiaryColor: '#EB7074',
        quarternaryColor: '#7F53FB',
        disabledDots: 'grey',
    },
    textColor: {
        primaryColor: '#3A3A3A',
        secondaryColor: '#818181',
        tertiary: '#717171',
        tertiary2: '#E5E5E5ff',
        quarternary: '#4A4A4A',
        pentaColor: '#C94B4F',
        hexaColor: '#474747',
        wrongNumber: '#C94B4F',
        resendOtp: '#AAAAAA',
        otpInput: '#4A4A4A',
        sessionProfileText: '#777684',
        appliedText: '#9570FD',
        activeTextColor: '#E8B459',
        appliedTextColor: '#9976FD',
    },
    listSeperator: {
        primary: '#E5E5E5',
    }

}

// 100% — FF
// 95% — F2
// 90% — E6
// 85% — D9
// 80% — CC
// 75% — BF
// 70% — B3
// 65% — A6
// 60% — 99
// 55% — 8C
// 50% — 80
// 45% — 73
// 40% — 66
// 35% — 59
// 30% — 4D
// 25% — 40
// 20% — 33
// 15% — 26
// 10% — 1A
// 5%  — 0D
// 0% —  00
