/**
 * 
 * Assets which will be used in the complete application should be declared in this file.
 */
export default {
    common: {
        back: require('./assets/common/back_icon.png'),
        noInternet: require('./assets/common/NoInternet.png'),
        dropDown: require('./assets/common/drop_down.png'),
        cross_white: require('./assets/common/union.png'),
        logo: require('./assets/common/affle_logo.png'),
        time: require('./assets/common/time.png'),
        date: require('./assets/common/date.png'),
        calendar: require('./assets/common/calendar.gif'),
        bottom_sheet: require('./assets/common/bottom_sheet.gif'),
        carousal: require('./assets/common/carousal.gif'),
        HorizontalStepIndicator: require('./assets/common/HorizontalStepIndicator.gif'),

    },
    authentication: {
        logo: require('./assets/authentication/logo.png'),
        verify_otp: require('./assets/authentication/verify_otp.png'),
        location_perm: require('./assets/authentication/location_artwork.png'),
        verify_otp2: require('./assets/authentication/verify_otp2.png'),
        document: require('./assets/authentication/document.png'),
        waiting_for_approval: require('./assets/authentication/waiting_for_approval.png'),
        upload_icon: require('./assets/authentication/upload_icon.png'),
        signup_header: require('./assets/authentication/signup_header.png'),
    }
}