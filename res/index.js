import Assets from "./Assets";
import Color from './Color'
import Constant from './Constants'
import Dimen from './Dimen'
import Strings from './String'
import URL from './URL'
import AsyncStorageValues from './AsyncStorageValues'
import Styles from './Styles'

// Exorting all the resources from index To keep track of all the resource files
export { Assets, Color, Constant, Dimen, Strings, URL, AsyncStorageValues, Styles}