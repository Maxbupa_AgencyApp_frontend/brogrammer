/**
 *
 * Constants which will be used for toast messages should be declared in this file.
 */
export default {
    common: {
        noInternetError: 'Please check your internet connection',
    },
    routeName: {
        USERTYPE: 'UserTypeScreen',
        INTRO_SCREEN: 'IntroScreen',
        INTRO_SCREEN_SELLER: 'IntroScreenSeller',
        SelectLanguage: 'SelectLanguage',
        LOGIN: 'Login',
        VerifyOtp: 'VerifyOtp',
        LocationPermission: 'LocationPermission',
        HomeScreen: 'HomeScreen',
        FSSAI_Document_Contactus: 'FSSAI_Document_Contactus',
        WaitingForApproval: 'WaitingForApproval',
        SignupSeller: 'SignupSeller',
        SignupSellerKitchenDetail1: 'SignupSellerKitchenDetail1',
        SignupSellerKitchenDetail2: 'SignupSellerKitchenDetail2',
        WebView: 'WebView'
    },
    regex: {
        email: /^(([^<>()\[\]\\.,;:-\s@"]+(\.[^<>()\[\]\\.,;:-\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        alphabet: /^[A-Z. ]+$/i
    },
}