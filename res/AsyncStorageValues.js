export default {
    lang_seleced:'IS_LANGUAGE_SELECTED',
    USER_TYPE_SELECTED:'USERTYPE',
    intro: 'IS_INTRO_COMPLETED',
    token: 'token',
    userData: 'userData',
    isTutor: 'isTutor',
    userDetails: 'userDetails',
    countryName: 'countryName',
    countryObj: 'countryObj'

}