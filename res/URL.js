/**
 * 
 * URL which will be used for API calling should be declared in this file.
 */

export const AppInfo = {
    baseUrlAPI: 'http://34.86.252.172:3000',
    apiVersion: 'v1',
    app: 'app/user/',
    serviceTimeOut: 20000,
    buyer: 'buyer',
    seller: 'seller'
};


export default apis = {
    getRequest: 'GET',
    postRequest: 'POST',
    deleteRequest: 'DELETE',
    putRequest: 'PUT',
    baseURL: AppInfo.baseUrlAPI + "/" + AppInfo.apiVersion + "/" + AppInfo.app,

    dummy_api: '/web/getTnC',
    login: 'otp/signin',
    verifyOtpBuyer: 'buyer/login',
    verifyOtpSeller: 'seller/login',
    sellerProfile: 'seller/profile',
    restaurantImages: 'seller/upload/restaurantImages',
    upload_fssai: 'seller/upload/fssai'
}
